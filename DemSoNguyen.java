package DemSo;

import java.util.Scanner;

public class DemSoNguyen {
    //Đọc số dưới hai mươi
    private static final String[] ones = {
            "", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín", "mười",
            "mười một", "mười hai", "mư ba", "mười bốn", "mười lăm", "mười sáu", "mười bảy", "mười tám", "mười chín"
    };

    //Đọc số từ hai mươi trở lên
    private static final String[] tens = {
            "", "", "hai mươi", "ba mươi", "bốn mươi", "năm mươi", "sáu mươi", "bày mươi", "tám mươi", "chín mươi"
    };
    public static String demHangChuc(long number){
        String words = "";
        if (number > 0) {
            //Đọc số dưới hai mươi
            if (number < 20) {
                words += ones[(int) number];
            }
            //Đọc số từ hai mươi trở lên
            else {
                words += tens[(int) (number / 10)];
                if ((number % 10) > 0) {
                    if (number % 10 == 5) {
                        words += " lăm";
                    } else if (number % 10 == 1) {
                        words += " mốt";
                    } else if (number % 10 == 4) {
                        words += " tư";
                    } else {
                        words += " " + ones[(int) (number % 10)];
                    }
                }
            }
        }
        return words;
    }
    public static String demHangTram(long number) {
        String words = "";
        if (number< 100){
            words += demHangChuc(number);
        }
        if ((number / 100) > 0) {
            words += demHangChuc(number / 100) + " trăm " + demHangChuc(number % 100);
        }
        return words;
    }

    public static String demHangNghin(long number) {
        String words = "";
        if ((number / 1000) > 0) {
            words += demHangTram(number / 1000) + " nghìn " + demHangTram(number % 1000);
        }
        return words;
    }
    public static String demHangTrieu(long number) {
        String words = "";
        if ((number / 1000000) > 0) {
            words += demHangTram(number / 1000000) + " triệu " + demHangNghin(number % 1000000);
        }
        return words;
    }
    public static String demHangTy(long number) {
        String words = "";
        if ((number / 1000000000) > 0) {
            words += demHangTram(number / 1000000000) + " tỷ " + demHangTrieu(number % 1000000000);
        }
        return words;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Nhập số: ");
            long number = scanner.nextLong();
            if (number <1000){
                System.out.println(demHangTram(number));
            } else if (number > 1000 && number < 100000) {
                System.out.println(demHangNghin(number));
            } else if (number > 1000000 && number < 1000000000) {
                System.out.println(demHangTrieu(number));
            }  else if (number > 1000000000) {
                System.out.println(demHangTy(number));
            }
        }
    }
}

