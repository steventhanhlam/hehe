package DemSo;

import java.util.Scanner;

public class ConvertNumberToWords {
    //Đọc số dưới hai mươi
    private static final String[] ones = {
            "", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín", "mười",
            "mười một", "mười hai", "mư ba", "mười bốn", "mười lăm", "mười sáu", "mười bảy", "mười tám", "mười chín"
    };

    //Đọc số từ hai mươi trở lên
    private static final String[] tens = {
            "", "", "hai mươi", "ba mươi", "bốn mươi", "năm mươi", "sáu mươi", "bày mươi", "tám mươi", "chín mươi"
    };

    public static String convert(int number) {
        if (number == 0) {
            return "không";
        }

        if (number < 0) {
            return "âm " + convert(Math.abs(number));
        }

        String words = "";

        //Đọc số triệu
        if ((number / 1000000) > 0) {
            words += convert(number / 1000000) + " triệu ";
            number %= 1000000;
        }

        //Đọc số nghìn
        if ((number / 1000) > 0) {
            words += convert(number / 1000) + " nghìn ";
            number %= 1000;
        }

        //Đọc số trăm
        if ((number / 100) > 0) {
            words += convert(number / 100) + " trăm ";
            number %= 100;
        }


        if (number > 0) {
            //Đọc số dưới hai mươi
            if (number < 20) {
                words += ones[number];
            }
            //Đọc số từ hai mươi trở lên
            else {
                words += tens[number / 10];
                if ((number % 10) > 0) {
                    if (number % 10 == 5) {
                        words += " lăm";
                    } else if (number % 10 == 1) {
                        words += " mốt";
                    } else if (number % 10 == 4) {
                        words += " tư";
                    }
                    else {
                        words += " " + ones[number % 10];
                    }
                }
            }
        }

        return words.trim();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Nhập số: ");
            int number = scanner.nextInt();
            System.out.println("Kết quả: " + convert(number));
        }
    }
}

